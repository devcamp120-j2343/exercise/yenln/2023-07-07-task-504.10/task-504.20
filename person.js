
class Person {
    constructor(personName, personAge, gender) {
        this.personName = personName;
        this.personAge = personAge;
        this.gender = gender;
    }
    getPersonInfo(){
        return console.log(this.personName + " " + this.personAge + " " + this.gender);
    }
}
export default Person;