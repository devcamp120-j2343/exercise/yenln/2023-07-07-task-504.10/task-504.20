import Person from "./person.js";
class Employee extends Person{
    constructor(personName, personAge, gender,employer, salary,position) {
        super(personName, personAge, gender);
        this.employer = employer;
        this.salary = salary;
        this.position = position;
    }
    getPersonInfo(){
        return console.log(this.personName + " " + this.personAge + " " + this.gender);
    }
    getEmployeInfo(){
        return console.log(`employer: ${this.employer}`);
    }
    getSalary(){
        return console.log(`salary: ${this.salary}`);
    }
    setSalary(){
        this.salary = 2000;
        return console.log(`new salary: ${this.salary}`);
    }
    setPosition(){
        return this.position = "Manager";
    }
    getPosition(){
        return console.log(`Position: ${this.position}`);
    }

}

export default Employee;