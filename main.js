import Person from "./person.js";
import Employee from "./employee.js";
var person1 = new Person('Jenny',18,'female');
console.log(person1.getPersonInfo());
console.log(person1 instanceof Person);

var employer1 = new Employee('Jenny',18,'female',"abc",1000,"staff");
console.log(employer1.getPersonInfo());
console.log(employer1.getEmployeInfo());
console.log(employer1.getSalary());
console.log(employer1.setSalary());
console.log(employer1.setPosition());
console.log(employer1.getPosition());

console.log(employer1 instanceof Person);
